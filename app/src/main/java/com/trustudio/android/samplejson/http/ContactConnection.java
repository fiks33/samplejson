package com.trustudio.android.samplejson.http;

import android.util.Log;

import com.trustudio.android.samplejson.model.Contact;
import com.trustudio.android.samplejson.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Default on 8/18/2015.
 */
public class ContactConnection extends RestConnection{
    public ArrayList<Contact> getInfoLiveStream() throws Exception {
        ArrayList<Contact> list = new ArrayList<>();

        try{
            String url = "http://api.androidhive.info/contacts/";
            InputStream is = connectGet(url);

            if(is != null){
                String response = Util.streamToString(is);
                Log.d(response, response);
                JSONObject JsonObj = (JSONObject) new JSONTokener(response).nextValue();


                JSONArray Json = JsonObj.getJSONArray("contacts");

                int length 			= Json.length();
                if (length > 0) {
                    for (int i = 0; i < length; i++) {
                        JSONObject c = Json.getJSONObject(i);
                        Contact data = new Contact();

                        data.name = c.getString("name");
                        data.email = c.getString("email");

                        list.add(data);
                    }
                }
                is.close();
            }
        } catch (RestException e) {
            throw e;
        }

        return list;
    }
}