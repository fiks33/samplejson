package com.trustudio.android.samplejson.http;

/**
 * Created by Default on 6/3/2015.
 */
public class RestException extends Exception {
    private static final long serialVersionUID = 1L;

    public RestException(String msg) {
        super(msg);
    }
}
