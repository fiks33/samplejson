package com.trustudio.android.samplejson.http;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Default on 6/3/2015.
 */

public class RestConnection {
    public RestConnection() {}

    public InputStream connectGet(String url) throws RestException {
        HttpClient httpClient 	= new DefaultHttpClient();
        HttpGet httpGet 		= new HttpGet(url);
        InputStream stream		= null;

        Log.d("SampleJson", "GET " + url);

        HttpResponse httpResponse;

        try {
            httpResponse 			= httpClient.execute(httpGet);
            HttpEntity httpEntity   = httpResponse.getEntity();

            if (httpEntity == null) throw new RestException("");

            stream	= httpEntity.getContent();

        } catch (Exception e) {
            throw new RestException("Connection error");
        }

        return stream;
    }

    public InputStream connectPost(String url, List<NameValuePair> params) throws RestException {
        HttpClient httpClient 	= new DefaultHttpClient();
        HttpPost httpPost 		= new HttpPost(url);
        InputStream stream		= null;

        Log.d("SampleJson", "POST " + url);

        HttpResponse httpResponse;

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            httpResponse		  = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            if (httpEntity == null) throw new RestException("");

            stream = httpEntity.getContent();

        } catch (Exception e) {
            throw new RestException("Connection error");
        }

        return stream;
    }
}