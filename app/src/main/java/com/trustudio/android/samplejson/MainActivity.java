package com.trustudio.android.samplejson;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.trustudio.android.samplejson.adapter.ContactAdapter;
import com.trustudio.android.samplejson.http.ContactConnection;
import com.trustudio.android.samplejson.model.Contact;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {

    private ListView mListview;
    ArrayList<Contact> mList = new ArrayList<Contact>();
    private ContactAdapter mAdapter;
    private ContactConnection rest;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListview = (ListView) findViewById(R.id.listView);

        mAdapter            = new ContactAdapter(this);
        rest                = new ContactConnection();

        new setList().execute();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class setList extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String result = "error";

            try {

                mList = rest.getInfoLiveStream();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mAdapter.setArray(mList);
            mListview.setAdapter(mAdapter);
        }
    }
}